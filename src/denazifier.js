function denazifier (node) {

  const tagName = node.tagName ? node.tagName.toLowerCase() : ''

  if (tagName === 'input' || tagName === 'textarea') {
    return
  }

  switch (node.nodeType) {
    case 1: // Element
    case 9: // Document
    case 11: // Document fragment
      let child = node.firstChild
      while (child) {
        const next = child.nextSibling
        denazifier(child)
        child = next
      }
      break

    case 3: // Text node
      if (node.parentNode && node.parentNode.hasAttribute('contenteditable')) {
        return
      }
      denazify(node)
      break
  }
}

function denazify (textNode) {
  let v = textNode.nodeValue

  v = v.replace(/в Укр/g, 'на Укр')
  v = v.replace(/В Укр/g, 'На Укр')

  v = v.replace(/В/g, 'V')
  v = v.replace(/([а-я]) в ([а-я])/g, "$1V$2")
  v = v.replace(/в/g, 'v')

  v = v.replace(/З/g, 'Z')
  v = v.replace(/з/g, 'z')

  v = v.replace(/Укр/g, 'Уср')
  v = v.replace(/укр/g, 'уср')

  textNode.nodeValue = v
}

denazifier(document.body)

const observer = new MutationObserver((mutations) => {
  for (const mutation of mutations) {
    for (const addedNode of mutation.addedNodes) {
      denazifier(addedNode)
    }
  }
})

observer.observe(document.body,
  {
    childList: true,
    subtree: true
  })
